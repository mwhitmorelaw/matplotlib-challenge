# Pymaceuticals Inc. Animal Study
This Jupyter Notebook is intended to analyze data from Pymaceuticals Inc.'s  most recent animal study. 
In this study, 250 mice identified with SCC tumor growth were treated through a variety of drug regimens. 
Over the course of 45 days, tumor development was observed and measured. 
The purpose of this study was to compare the performance of Pymaceuticals' drug of interest, Capomulin, versus the other treatment regimens. The notebook provides all of the tables and figures needed for the technical report of the study. 
It also provides a top-level summary of the study results.

## Getting Started

From the project root, navigate to the 'Pymaceuticals' directory. There, you will find Jupyter notebook 'pymaceuticals.ipynb'.
The notebook file can be run in Jupyter with following command line statement: 'jupyter-notebook.exe .\pymaceuticals.ipynb'.
Once the notebook is open in Jupyter, select the first cell and press SHIFT + ENTER to run it.
This will also move the cell selection to the next cell down. The remainder of the notebook results
can be viewed by continuing to press SHIFT + ENTER in each cell.

### Prerequisites

This project requires Jupyter Notebook to be installed. Installation instructions can be found [here](https://jupyter.org/install).

### Installing

This project relies on the following dependencies:
- scipy==1.1.0
- pandas==0.23.4
- matplotlib==3.0.2
- numpy==1.15.4

These packages can be installed via pip or Anaconda

## Running the tests

Automated tests are included in the Jupyter Notebook file in the form of assert statements. None of the 
assert statements should fail as you execute the notebook.

### Break down into end to end tests

The scripts included in this project are self-contained. The assert statements act as end-to-end tests.

## Deployment

The scripts included in this project are self-contained and intended to be used for one-off runs. Assuming you feel these scripts should be deployed to production, no additional steps are needed beyond those stated in 'Getting Started' above.

## Built With

* [Python](http://www.python.org) - Language used
* [Jupyter](https://jupyter.org) - IDE used
* [Pandas](https://pandas.pydata.org) - Data Manipulation Library
* [Matplotlib](https://matplotlib.org) - Data Visualization Library
* [Numpy](https://numpy.org) - Data Manipulation Library
* [SciPy](https://www.scipy.org) - Statistics Library

## Versioning

We use [SemVer](http://semver.org/) for versioning. The version is currently 1.0.0.

## Authors

* **Michael Whitmore** - *All work* - [mwhitmorelaw](https://gitlab.com/mwhitmorelaw)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Guido van Rossum
